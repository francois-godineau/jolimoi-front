// xhr solution
import axios from 'axios'

const apiBase = `${process.env.BACKEND}/converter`

async function arabicToRoman(decimal) {
  return axios.post(`${apiBase}/arabic-to-roman`, {
    decimal: parseInt(decimal)
  })
}

export {
  arabicToRoman
}
