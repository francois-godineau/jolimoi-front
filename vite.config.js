import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import replace from '@rollup/plugin-replace'
import {resolve} from 'path'

export default defineConfig({
  plugins: [
    replace({
      preventAssignment: true,
      'process.env.ENV': JSON.stringify('dev'),
      'process.env.BACKEND': JSON.stringify('http://localhost:5000'),
    }),
    svelte(),
  ],
  resolve: {
    alias: {
      '@': resolve(__dirname, './src'),
    },
  },  
})
